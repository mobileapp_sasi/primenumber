import 'package:dart_application_1/dart_application_1.dart' as dart_application_1;
import 'dart:io';

void main() {
  print('Input number :');
  var number = int.parse(stdin.readLineSync()!);
  if(checkPrime(number)){
    print('$number is prime number');
  } else {
    print('$number is not prime number');
  }

}

bool checkPrime(num) {
  for (var i =2; i <= num/ i; i++) {
    if (num % i == 0) {
      return false;
    }
  }
  return true;
}